# viewcounter
ethical view counting api

## examples
### embedded view counter
```html
<div id="view-count"></div>
<script>
	var req = new XMLHttpRequest();
	req.addEventListener("load", function () {
		document.getElementById("view-count")
			.innerHTML = "<b>page views</b> " + JSON.parse(this.responseText).views;
	});
	req.open("POST", "/viewcounter");
	req.send("/index.html");
</script>
```

### requesting all view counts
sending a GET request will return a json object with all the stored viewcounts
```sh
$ curl localhost:8001/viewcounter
{"code":200,"/index.html":1,...}
```

### incrementing view counts/retrieving view count
to increment a view count, simply send a post request with the body containing the page path. the response will contain the view count
```sh
$ curl localhost:8001/viewcounter -d /index.html
{"code":200,"message":"counted page view","views":2}
```

if you have already incremented the view
```sh
$ curl localhost:8001/viewcounter -d /index.html
{"code":200,"message":"you have seen this page today","views":2}
```

## configuration
```js
{
	"port": 8001,
	"path": "/viewcounter",
	"site": "zvava.org",
	"countDB": ".count.json",
	"ratelimitDB": ".ratelimit.json",
	"saveTimeout": 5000
}
```

### `port`
port the service is hosted on

### `path`
path to listen on

eg. a path of `/api/viewcount` would respond to `http://ip:port/api/viewcount`

### `site`
site to validate urls against

eg. with a `site` of `example.com`, posting `/cheese.html` will check if `https://example.com/cheese.html` returns a 200 status code or not

### `countDB` + `ratelimitDB`
filename for both the view counts and last viewed dates of pages attributed to ips

if you see your `ratelimitDB` getting absurdly large i'd recommend deleting it every once in a while

### `saveTimeout`
period to elapse before an update in view count is saved to disk, this saves the storage from constant strain from frequent changes
